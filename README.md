# README #

* **Quick summary**
REG is native binary that one can use from Windows command line to edit registry.
This tool is not part of WinCE and RegCompact is a light replacement for it.
It handles ADD,DELETE,CREATE and QUERY functions.

* **Version**
0.1


### How do I get set up? ###

* **Deployment instructions**
Deploy it to your WinCE device where you plan to use it.
If you are building your own BSP image, put the executable in Windows directory so that it becomes accessible from cmd.exe.

### Usage ###

* **Create a key**

    `reg create MyApp`

* **Add a value**

   `reg add HKLM/MyApp /v Version /t REG_SZ /d 0.0`
*Or*
    `reg add MyApp /v Version /d 0.0`
        
* **Delete a key**

	`reg delete MyApp /va`

* **Delete a value**

    `reg delete MyApp /v Version`

* **Query a value**

    `reg query MyApp /v Version`

* **Help**

    `reg --help`
*Or*
    `reg -h`
*Or*
    `reg /?`
*Or* 
    `reg help`

### Limitations ###

* **Types**

At this day, REG_SZ and REG_DWORD are supported only

* **Root keys**

All root keys are supported. Use help for more information

* **Backslashes not supported**

If you want to set a path as a string, do not use backslash but slash :

`reg add MyApp /v Version /d /Hard Disk/App`
