﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace RegCompact
{
    /// <summary>
    /// Root registry key types
    /// </summary>
    internal enum RootKey
    {
        /// <summary>
        /// HKey_Local_machine
        /// </summary>
        HKLM,
        /// <summary>
        /// HKey_current_user
        /// </summary>
        HKCU,
        /// <summary>
        /// HKey_users
        /// </summary>
        HKU,
        /// <summary>
        /// HKey_classes_root
        /// </summary>
        HKCR,
    }
}
