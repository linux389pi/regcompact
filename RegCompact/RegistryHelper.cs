﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using Microsoft.Win32;

namespace RegCompact
{
    /// <summary>
    /// Generic access to resgistry
    /// </summary>
    internal class RegistryHelper
    {
        /// <summary>
        /// Retruns the handle to the specified root key
        /// </summary>
        /// <param name="root">Registry root type</param>
        /// <returns>Handle on the root key</returns>
        private RegistryKey GetRoot(RootKey root)
        {
            RegistryKey returnValue = Registry.LocalMachine;

            switch (root)
            {
                case RootKey.HKLM:
                    returnValue = Registry.LocalMachine;
                    break;
                case RootKey.HKCU:
                    returnValue = Registry.CurrentUser;
                    break;
                case RootKey.HKU:
                    returnValue = Registry.Users;
                    break;
                case RootKey.HKCR:
                    returnValue = Registry.ClassesRoot;
                    break;
            }
            return returnValue;
        }

        /// <summary>
        /// Creates a new key
        /// </summary>
        /// <param name="root">Registry root</param>
        /// <param name="key">Key name</param>
        public void CreateKey(RootKey root, string key)
        {
            RegistryKey rootkey = GetRoot(root);
            RegistryKey regkey;

            // opens key in writable mode
            regkey = rootkey.CreateSubKey(key);
            regkey.Close();
        }

        /// <summary>
        /// Creates a new value under the specified key
        /// </summary>
        /// <param name="root">Registry root</param>
        /// <param name="key">Key name</param>
        /// <param name="subkey">Value name</param>
        public void CreateValue(RootKey root, string key, string subkey)
        {
            RegistryKey rootkey = GetRoot(root);
            RegistryKey regkey;

            // opens key in writable mode
            regkey = rootkey.OpenSubKey(key, true);

            if (regkey != null)
            {
                regkey.CreateSubKey(subkey);
                regkey.Close();
            }
        }

        /// <summary>
        /// Deletes a key and all its values
        /// </summary>
        /// <param name="root">Registry root</param>
        /// <param name="node">Key name</param>
        public void DeleteNode(RootKey root, string node)
        {
            RegistryKey rootkey = GetRoot(root);
            rootkey.DeleteSubKeyTree(node);
        }

        /// <summary>
        /// Deletes a value
        /// </summary>
        /// <param name="root">Registry root</param>
        /// <param name="key">Key name</param>
        /// <param name="subkey">Value name</param>
        public void DeleteSubkey(RootKey root, string key, string subkey)
        {
            RegistryKey rootkey = GetRoot(root);
            RegistryKey regkey;

            // opens key in writable mode
            regkey = rootkey.OpenSubKey(key, true);

            if (regkey != null)
            {
                regkey.CreateSubKey(subkey);
                regkey.Close();
            }
        }

        /// <summary>
        /// Add or set a value
        /// </summary>
        /// <param name="root">Registry root</param>
        /// <param name="key">Key name</param>
        /// <param name="subkey">Value name</param>
        /// <param name="value">Data</param>
        public void SetRegistryKey(RootKey root, string key, string subkey, object value)
        {
            RegistryKey rootkey = GetRoot(root);
            RegistryKey regkey;

            // opens key in writable mode
            regkey = rootkey.OpenSubKey(key, true);

            // check key then
            if (regkey != null)
            {

                regkey.SetValue(subkey, value);
                regkey.Close();
            }
        }

        /// <summary>
        /// Queries a value
        /// </summary>
        /// <param name="root">Registry root</param>
        /// <param name="key">Key name</param>
        /// <param name="subkey">Value name</param>
        /// <returns>Data of the value</returns>
        public object GetRegistryKey(RootKey root, string key, string subkey)
        {
            RegistryKey rootkey = GetRoot(root);
            RegistryKey regkey;
            object value = default(object);

            // opens key in writable mode
            regkey = rootkey.OpenSubKey(key, false);

            // check key then
            if (regkey != null)
            {
                value = regkey.GetValue(subkey, value);
                regkey.Close();
            }

            return value;
        }

        /// <summary>
        /// Checks if a key exists
        /// </summary>
        /// <param name="root">Registry root</param>
        /// <param name="key">Key name</param>
        /// <returns>true if it exists, false otherwise</returns>
        public bool Exists(RootKey root, string key)
        {
            bool exist = false;
            RegistryKey rootkey = GetRoot(root);
            RegistryKey regkey = rootkey.OpenSubKey(key, false);

            if (regkey != null)
            {
                exist = true;
                regkey.Close();
            }

            return exist;
        }

        /// <summary>
        /// Checks if a value exists
        /// </summary>
        /// <param name="root">Registry root</param>
        /// <param name="key">Key name</param>
        /// <param name="value">Value name</param>
        /// <returns>true if it exists, false otherwise</returns>
        public bool Exists(RootKey root, string key, string value)
        {
            bool exist = false;
            RegistryKey rootkey = GetRoot(root);
            RegistryKey regkey = rootkey.OpenSubKey(key, false);

            if (regkey != null)
            {
                object valueData = regkey.GetValue(value);
                if (valueData != null)
                {
                    exist = true;
                    regkey.Close();
                }
            }

            return exist;
        }
    }
}
