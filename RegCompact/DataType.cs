﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace RegCompact
{
    /// <summary>
    /// Registry data types
    /// </summary>
    enum DataType
    {
        /// <summary>
        /// String (default)
        /// </summary>
        REG_SZ,
        /// <summary>
        /// Dword integer
        /// </summary>
        REG_DWORD,
    }
}
