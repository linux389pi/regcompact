﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;

namespace RegCompact
{
    class Program
    {
        /// <summary>
        /// Registry accessor
        /// </summary>
        static RegistryHelper registry = new RegistryHelper();

        /// <summary>
        /// Handles REG ADD command
        /// </summary>
        /// <param name="args">List of user arguments</param>
        static void HandleAdd(string[] args)
        {
            try
            {
                // The following regex capture the following groups
                // Group 1: Root Key
                // Group 2: RegKey
                // Group 3: ValueName
                // Group 4: ValueType
                // Group 5: Data
                const string validRegex = @"ADD\s(HKLM|HKCU|HKU|HKCR)*\/?(\w+)(?:\s\/v\s)(\w+)(?:(?:\s\/t\s)(REG_SZ|REG_DWORD))*(?:(?:\s\/d\s)(.+))*";
                string commandline = String.Join(" ", args, 0, args.Length);
                if (Regex.IsMatch(commandline, validRegex, RegexOptions.IgnoreCase))
                {
                    MatchCollection matches = Regex.Matches(String.Join(" ", args, 0, args.Length), validRegex, RegexOptions.IgnoreCase);

                    // Parse options
                    // Root
                    RootKey root = RootKey.HKLM;
                    if (String.IsNullOrEmpty(matches[0].Groups[1].Value) == false)
                    {
                        root = (RootKey)Enum.Parse(typeof(RootKey), matches[0].Groups[1].Value, true);
                    }
                    // RegKey
                    string regKey = matches[0].Groups[2].Value;
                    if (registry.Exists(root, regKey))
                    {
                        // ValueName
                        string subKey = matches[0].Groups[3].Value;

                        // ValueType 
                        DataType type = DataType.REG_SZ;
                        if (String.IsNullOrEmpty(matches[0].Groups[4].Value) == false)
                        {
                            type = (DataType)Enum.Parse(typeof(DataType), matches[0].Groups[4].Value, true);
                        }
                        // Value
                        object value = default(object);
                        switch (type)
                        {
                            case DataType.REG_SZ:
                                value = matches[0].Groups[5].Value;
                                break;
                            case DataType.REG_DWORD:
                                if (String.IsNullOrEmpty(matches[0].Groups[5].Value))
                                {
                                    value = 0;
                                }
                                else
                                {
                                    value = Int32.Parse(matches[0].Groups[5].Value);
                                }
                                break;
                        }
                        // Finally add value
                        registry.SetRegistryKey(root, regKey, subKey, value);
                    }
                    else
                    {
                        Console.WriteLine("ERR: {0}/{1} does not exist",root,regKey);
                    }
                }
                else
                {
                    Console.WriteLine("ERR: ADD is not formatted properly");
                }
            }
            catch (ArgumentException)
            {
                Console.WriteLine("ERR: Failed to add value. Ensure that you are using valid DataType and Root key");
            }
        }

        /// <summary>
        /// Handles REG CREATE command
        /// </summary>
        /// <param name="args">List of user arguments</param>
        static void HandleCreate(string[] args)
        {
            try
            {
                // The following regex capture the following groups
                // Group 1: Root Key
                // Group 2: RegKey
                const string validRegex = @"CREATE\s(HKLM|HKCU|HKU|HKCR)*\/?(\w+)";
                string commandline = String.Join(" ", args, 0, args.Length);
                if (Regex.IsMatch(commandline, validRegex, RegexOptions.IgnoreCase))
                {
                    MatchCollection matches = Regex.Matches(String.Join(" ", args, 0, args.Length), validRegex, RegexOptions.IgnoreCase);

                    // Parse options
                    // Root
                    RootKey root = RootKey.HKLM;
                    if (String.IsNullOrEmpty(matches[0].Groups[1].Value) == false)
                    {
                        root = (RootKey)Enum.Parse(typeof(RootKey), matches[0].Groups[1].Value, true);
                    }
                    // RegKey
                    string regKey = matches[0].Groups[2].Value;
                    if (registry.Exists(root, regKey))
                    {
                        Console.WriteLine("WARN: {0}/{1} already exists", root, regKey);
                    }
                    else
                    {
                        // Create key
                        registry.CreateKey(root, regKey);
                    }
                }
                else
                {
                    Console.WriteLine("ERR: CREATE is not formatted properly");
                }
            }
            catch (ArgumentException)
            {
                Console.WriteLine("ERR: Failed to create key. Ensure that you are using a valid Root key");
            }
        }

        /// <summary>
        /// Handles REG QUERY command
        /// </summary>
        /// <param name="args">List of user arguments</param>
        static void HandleQuery(string[] args)
        {
            try
            {
                // The following regex capture the following groups
                // Group 1: Root Key
                // Group 2: RegKey
                // Group 3: ValueName
                const string validRegex = @"QUERY\s(HKLM|HKCU|HKU|HKCR)*\/?(\w+)(?:\s\/v\s(\w+))?";
                string commandline = String.Join(" ", args, 0, args.Length);
                if (Regex.IsMatch(commandline, validRegex, RegexOptions.IgnoreCase))
                {
                    MatchCollection matches = Regex.Matches(String.Join(" ", args, 0, args.Length), validRegex, RegexOptions.IgnoreCase);

                    // Parse options
                    // Root
                    RootKey root = RootKey.HKLM;
                    if (String.IsNullOrEmpty(matches[0].Groups[1].Value) == false)
                    {
                        root = (RootKey)Enum.Parse(typeof(RootKey), matches[0].Groups[1].Value, true);
                    }
                    // RegKey
                    string regKey = matches[0].Groups[2].Value;
                    if (registry.Exists(root, regKey))
                    {
                        // ValueName
                        if (String.IsNullOrEmpty(matches[0].Groups[3].Value) == false)
                        {
                            string subkey = matches[0].Groups[3].Value;

                            if (registry.Exists(root, regKey, subkey))
                            {
                                // Query value
                                object value = registry.GetRegistryKey(root, regKey, subkey);
                                Console.WriteLine("{0}", value);
                            }
                            else
                            {
                                Console.WriteLine("WARN: {0}/{1}/{2} does not exist", root, regKey, subkey);
                            }
                        }
                        else
                        {
                            Console.WriteLine("WARN: QUERY is not formatted properly");
                        }
                    }
                    else
                    {
                        Console.WriteLine("WARN: {0}/{1} does not exist", root, regKey);
                    }
                }
                else
                {
                    Console.WriteLine("ERR: QUERY is not formatted properly");
                }
            }
            catch (ArgumentException)
            {
                Console.WriteLine("ERR: Failed to query value.");
            }
        }

        /// <summary>
        /// Handles REG DELETE command
        /// </summary>
        /// <param name="args">List of user arguments</param>
        static void HandleDelete(string[] args)
        {
            try
            {
                // The following regex capture the following groups
                // Group 1: Root Key
                // Group 2: RegKey
                // Group 3: ValueName
                // Group 4: /va option
                const string validRegex = @"DELETE\s(HKLM|HKCU|HKU|HKCR)*\/?(\w+)(?:\s\/v\s(\w+)|\s(\/va))?";
                string commandline = String.Join(" ", args, 0, args.Length);
                if (Regex.IsMatch(commandline, validRegex, RegexOptions.IgnoreCase))
                {
                    MatchCollection matches = Regex.Matches(String.Join(" ", args, 0, args.Length), validRegex, RegexOptions.IgnoreCase);

                    // Parse options
                    // Root
                    RootKey root = RootKey.HKLM;
                    if (String.IsNullOrEmpty(matches[0].Groups[1].Value) == false)
                    {
                        root = (RootKey)Enum.Parse(typeof(RootKey), matches[0].Groups[1].Value, true);
                    }
                    // RegKey
                    string regKey = matches[0].Groups[2].Value;
                    if (registry.Exists(root, regKey))
                    {
                        // ValueName
                        if (String.IsNullOrEmpty(matches[0].Groups[3].Value) == false)
                        {
                            string subkey = matches[0].Groups[3].Value;

                            if(registry.Exists(root,regKey,subkey))
                            {
                                // Delete value
                                registry.DeleteSubkey(root, regKey, subkey);
                            }
                            else
                            {
                                Console.WriteLine("WARN: {0}/{1}/{2} does not exist", root, regKey, subkey);
                            }
                        }
                        else if (String.Equals(matches[0].Groups[4].Value, "/va") == false)
                        {
                            // Delete the reg key
                            registry.DeleteNode(root, regKey);
                        }
                        else
                        {
                            Console.WriteLine("WARN: DELETE is not formatted properly");
                        }
                    }
                    else
                    {
                        Console.WriteLine("WARN: {0}/{1} does not exist", root, regKey);
                    }
                }
                else
                {
                    Console.WriteLine("ERR: DELETE is not formatted properly");
                }
            }
            catch (ArgumentException)
            {
                Console.WriteLine("ERR: Failed to create key.");
            }
        }

        /// <summary>
        /// Prints help about REG command
        /// </summary>
        static void PrintHelp()
        {
            string command_helper = "REG Light for WinCE\n\n"+
                "Syntax:\n\n\t"+
                "REG QUERY [ROOT/]RegKey /v ValueName\n\t"+
                "REG ADD [ROOT/]RegKey /v ValueName [/t DataType] [/d Data]\n\t"+
                "REG DELETE [ROOT/]RegKey /v ValueName\n\t"+
                "REG DELETE [ROOT/]RegKey /va -- Removes all values under this key\n\t"+
                "REG CREATE [ROOT/]RegKey\n\n"+
                "Key:\n\tROOT:\n\t\t"+
                "HKLM = HKey_Local_machine (default)\n\t\t"+
                "HKCU = HKey_current_user\n\t\t"+
                "HKU  = HKey_users\n\t\t"+
                "HKCR = HKey_classes_root\n\t"+
                "ValueName : The value, under the selected RegKey, to edit.\n\t"+
                "/d Data : The actual data to store as String, integer, etc\n\t"+
                "/t DataType : REG_SZ (default) | REG_DWORD \n\t";
            Console.WriteLine(command_helper);
        }

        static void Main(string[] args)
        {
            if (args.Length > 0)
            {
                switch (args[0].ToLower())
                {
                    case "add":
                        HandleAdd(args);
                        break;
                    case "create":
                        HandleCreate(args);
                        break;
                    case "query":
                        HandleQuery(args);
                        break;
                    case "delete":
                        HandleDelete(args);
                        break;
                    case "-h":
                    case "--help":
                    case "help":
                    case "/?":
                        PrintHelp();
                        break;
                    default:
                        Console.WriteLine(String.Format("{0} is not supported", args[0]));
                        break;
                }
            }
            else
            {
                PrintHelp();
            }
        }
    }
}
